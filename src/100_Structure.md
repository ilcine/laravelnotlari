# Laravel structure

[laravel 8 structure link:](https://laravel.com/docs/8.x/structure)

> _Emrullah İLÇİN_


## MVC structure

* [130 ROUTE:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/130_ROUTE.md)
* [140 MODEL:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/140_MODEL.md)
* [150 CONTROLLER:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/150_CONTROLLER.md)
* [160 VIEW:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/160_VIEW.md)

## Laravel process path

1. __first begin public/index.php__<br>
 '/../storage/framework/maintenance.php' <br>
 require /vendor/autoload.php'; --> 'vendor/composer/autoload_real.php'; <br>
 $app = require_once '/bootstrap/app.php';<br>
 $kernel = $app->make(Kernel::class);<br>
 $kernel->terminate($request, $response); 
 &nbsp; <blockquote> Apache's `.htaccess` process runs on the directory</blockquote>

2. **If the "/" path is in "/routers/web.php" or "routers/api.php"** <br>
  ex: Route::get('/', function () { return view('welcome'); }); // go to --> resource/view/welcome.blade.php<br>
  __else__ <br>
  go to: resources/view/app.blade.php 
 &nbsp; <blockquote> files contain initial comments of html, js and css. </blockquote>
 
3. **example app.blade.php**<br>
 &nbsp; <blockquote>
    ```
    <!doctype html>
     <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
     <head>
       <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="csrf-token" content="{{ csrf_token() }}">
       <title>{{ config('app.name', 'Laravel') }}</title>
       <script src="{{ asset('js/app.js') }}" defer></script>
       <link rel="dns-prefetch" href="//fonts.gstatic.com">
       <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
       <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     </head>
     <body>
       <div id="app">
       </div>
     </body>
     </html>
    ```
 </blockquote>
 
4. **composer.json** this file contains Dependency Management definitions for PHP<br>
  _ex: xyz/def packages_ <br>
 `composer require xyz/def` # xyz/dev package is installed<br>
 `composer remove xyz/def` # xyz/dev package is removed<br>

5. **package.json** this file contains definitions for npm in JS <em>(npm is a package management system for node.js in javaScript)<em><br>
 - this execute in "/resources/js/app.js" file default);
 - if "App" component is written inside <em>ex: `Vue.component('App', require('./App.vue')` </em> and write `app.blade.js` file `<div id="app"> <App></App> </div>` component 
 - run `npm run dev` ; npm, writes `public/js/app.js` and `public/css/app.css` file with webpack process;
 - ex: `npm install xyz` and `npm uninstall xyz` # packages install or uninstall

6. **webpack.mix.js** Laravel Mix provides a fluent API for defining webpack build steps for your application using several common CSS and JavaScript pre-processors.
 - <em>Webpack, comes preinstalled with Laravel</em>  [see for install manually](src/101_webpack.md ) <br>
 ex:
 &nbsp; <blockquote>
    ```
    const mix = require('laravel-mix');
    mix.browserSync('proje1.ilcin.name.tr');
    mix.js('resources/js/app.js', 'public/js')
      .sass('resources/sass/app.scss', 'public/css')
      .copyDirectory('public/js', '../public/js')
      .copyDirectory('public/css', '../public/css');
    ```
 </blockquote>

## Directory structure 

| Directory              | Description                                                   |  
|--------------------|------------------------------------------------------------|
| ./.env  | DATABASE adı kullanıcı adı, şifresi, IP nosu,KEY,DEBUG,MAIL gibi tanımların yapıldığı dizin. |
| ./config/  | Laravel’de gerekli tüm config ayarlarının tutulduğu dizin. |
| ./routes/web.php  | Yönlendirmelerin yazıldığı dosya (ROUTE) |
| ./app/XXX.php  | Modelleme dosyaları (MODEL) | 
| ./app/Http/Controllers/  | Controller dizini (CONTROLLER) |
| ./database/migrations/  | Database tanımlarının yapıldığı dizin (MODEL için) | 
| ./resources/views/  | html dosyalarının konulacağı dizin  (VIEW için) |
| ./resources/assets/js/ | javascript framework ve diger js tanımları için **app.js** vb tanım dosyalarının tutulduğu dizi (npm kullanımlı için)|
| ./resources/assets/sass/  | css framework ve diger css tanımları için **app.scss** vb tanım dosyalarının tutulduğu dizin. (npm kullanımı için) |
| ./public/ | javascript ve css ve image lerin konulacağı dizin. |
| ./app/Http/Requests/ | form aracığıyla girilecek verilerin kontrolu bu dizine konulacak request config dosyaları ile yapılır. |
| ./storage/logs/laravel.log  |Laravel çalışırken oluşan işlemlerin kaydının tutulduğu dosya |
| ./app/Helpers/ | Bilgi girişi sırasında kontrolların yapılması bu dizine konulacak Helpers.php dosyasına yazılanlarla sağlanır.  |
| ./composer.json | Laravel’le birlikte gelen tüm eklentilerin ve bizim ilave olarak koyacağımız eklentilerin bulunacağı dosya.  |



@extends('layouts.appBasit')
@section('content')

<h2> Silineck </h2>

@if (session('status'))
	<p>{{ session('status') }}</p>
@endif

<form method="post" action="{!! action('TaskController@TaskSil', 
	encrypt($tasks->id)) !!}" >
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">

		<table style="width:50%">
			<tbody>
				<tr>
					<td> <p"><b>{!! $tasks->name !!} </b> </p></td> 		
					<td> <a href="{!! action('TaskController@TaskListesi', null) !!}" class="ghost-button" > Vazgeç </a></td> 
					<td> <button type="submit" class="ghost-button"> Kaydı Sil </button> </td>
				</tr>
			</tbody>
		</table>	 	
		
</form>
		 
@endsection



# Laravel Notes

Laravel is an open source PHP framework with MVC structure.
Object Orientation "Object Oriented Programming (OOP)";
"Object Relational Mapping (ORM)" in Object relationship with DB; "Composer" as dependency management; "Artisan" is used for configuration. In these notes, the subjects that will be needed about Laravel are explained with examples. <small><i> (Laravel, MVC yapısına sahip açık kaynak kodlu PHP çerçevesidir (framework). Nesne yönelimi "Object Oriented Programming(OOP)"; DB ile Nesne ilişkisinde "Object Relational Mapping(ORM)"; Bağımlılık yönetimi olarak "Composer"; Konfigurasyon için "Artisan" kullanılır. Bu notlarda Laravel hakkında ihtiyaç duyulacak konular örneklerle anlatılmıştır.) </small></i>

> _Emrullah İLÇİN_

* [First definitions for cloud or local Server:](https://)<br>
    - [Composer install on Ubuntu:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/800_Composer.md)
    - [NPM install on Ubuntu:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/810_NPM.md)
    - [Install PHP on Ubuntu and define in apache2:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/820_Install_php.md)
    - [Install Apache and Virtual Host on Ubuntu:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/830_ApacheVirtualHost.md)
    - [Install MySql on Ubuntu:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/840_mysql.md)
    - [Install PhpMyAdmin on Ubuntu:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/850_phpMyAdmin.md)
    - [Homestead Install on Windows](https://gitlab.com/ilcine/LaravelNotes/-/blob/master/src/011_Homestead.installation.md)
* [Laravel install:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/080_LaravelKurulum.md)
* [Laravel config:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/090_Config.md)
* [100 Laravel structure:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/100_Structure.md)
* [110 Laravel first page:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/110_first_page.md)
* [120 Artisan:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/120_Artisan.md)
* [130 Auth:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/130_Laravel_Auth.md)
* [140 Passport:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/250_passport.md)
* [170 Simple CRUD exampe:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/170_CRUD.md)
* [190 Vue:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/190_Vue.md)
* [200 Vue CRUD example:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/200_VueCRUD.md)
* [201 Vue-Router:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/201_Vue_Router.md)
* [210 Debug:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/210_Debug.md)
* [220 Request:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/220_request.md)
* [230 Validate:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/230_validate.md)
* [240 Session:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/240_session.md)
* [270 Page Not Found:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/270_PageNotFound.md)
* [280 Invite:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/280_invite.md)
* [290 Time Date:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/290_time_date.md)
* [291 Using the Double Underscore (__) Translation:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/291_Using_the_Double_Underscore_Translation.md)
* [292 Create console commands with artisan:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/292_Creating_Commands_with_PHP_Artisan.md)
* [293 How to Use Laravel Config Files:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/293_How_to_Use_Laravel_Config_Files.md)
* [294 Laravel Paths:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/294_Laravel_Path.md)
* [300 Href & url get post:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/300_href_&_url_get_post.md)
* [310 Laravel Package Development:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/310.Laravel_package_development.md)
* [320 DB Elequent and queries:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/320.DB.eloquent.queries.md)
* [321 DB HasMany and BelongsTo:](https://gitlab.com/ilcine/LaravelNotes/blob/master/src/321_DB_HasMany_BelongsTo.md)

